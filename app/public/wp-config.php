<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bjEfynW894kLe16f9JPDxnCiFzvbex2Awaf8M3lDVoyIRaS3el8t/8DkzFerENzAoyZRcdke9g4JSPkteNraCQ==');
define('SECURE_AUTH_KEY',  'aYsKghUeZcdo269OFzaDP6ERA8mXL0t2hlT6fonDmXknyPj3SPZky80n8c+cBePrv2tiKj0L7V6sxxHDuDsddg==');
define('LOGGED_IN_KEY',    'At14rhTyStF8fGN2yuxa4mBHFiL6aZhI2/NQXy0RyUAwmF8iX1Wal3ntxOo98Ud8GVZ6L4GzhCggKSEtz6/leA==');
define('NONCE_KEY',        'CgjyuffEC6rfTDkbqN7wygia2BGWv0gwx3qvgFGebL93GiONSNzkCXyOq9009krDEcXCjTkdHcSykVcA1V1m0A==');
define('AUTH_SALT',        'y6qekMOwcrz6e/CYJU84f68kNGQLXraAEOtNHQjeHN0PeQfGmdB3WcxFZ0w5Aeqt3MIuX29BqDIbkQ9T9z45ZA==');
define('SECURE_AUTH_SALT', 'T+IN+JZiic0VZ7DzmLb++lq1z5LMlVZIuxTcjBE1YUIaRsnO6HqsV7nofGOxb4TLSh9ucMiFXbHR/SiyA+mx0Q==');
define('LOGGED_IN_SALT',   'v5nyNziH1CWRpqTynRUgoUqgc0OHuhypL4X3nrbYuJt10PV9+ReItgfoHSzcTo7+bUEbdgOxuA7bKelrxATNPw==');
define('NONCE_SALT',       '4tZOjumku9RWsLJe3cxD+4ElHeZe8Gu9sZwg0IWk8vcKpSsxpMK0bCjmM9EdS9FfAdaGfnEfbMipzBEb8rT7Dg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
