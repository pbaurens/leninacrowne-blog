=== Pop Rock ===
Contributors: catchthemes
Tags: one-column, two-columns, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-image-header, featured-images, footer-widgets, full-width-template, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, blog, entertainment, portfolio
Requires at least: 5.1
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 2.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Pop Rock - A simple and clean Music WordPress theme for solo musicians and bands that aims at promoting their music.

== Description ==

Pop Rock - A simple and clean Music WordPress theme for solo musicians and bands that aims at promoting their music. Pop Rock is a child theme of My Music Band, a simple yet power-packed music WordPress theme. Pop Rock provides you with crucial features such as featured slider, featured content, playlist, hero content, portfolio, header media, and more. And with all the incredible music-related features included in the theme, Pop Rock will definitely give your musical website an awesome online presence. This will, in turn, will surely attract more audiences to your music. The theme focuses on creating a visually aesthetic musical website with zero requirements of coding. Your website will look powerful yet elegant on mobile devices and desktops, regardless of screen resolution with its responsive design. The theme is translation ready, which helps your fans across the globe to stay updated on your website as well. So, install Pop Rock today and let your music roar at large! Check out Theme Instructions at https://catchthemes.com/themes/pop-rock/#theme-instructions, Support at https://catchthemes.com/support/ and Demo at https://catchthemes.com/demo/pop-rock/

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Pop Rock in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to https://catchthemes.com/theme-instructions/pop-rock/ for a guide on how to customize this theme.
5. Navigate to Appearance -> Customize in your admin panel and customize to taste.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports all plugins from catchplugins.com and WooCommerce Plugin.

= Where can I find theme documentation? =

You can check our Theme Instructions at https://catchthemes.com/themes/pop-rock in the Theme Instructions Tab.

= Where can I find theme demo? =

You can check our Theme Demo at https://catchthemes.com/demo/pop-rock/

== Resources ==

* Images used are released under the CC0 license
	Screenshot & Header image
	* https://pixabay.com/en/people-singing-girl-beauty-guitar-2566065/
	* https://pixabay.com/en/fantasy-landscape-elephant-man-2995326/

== Changelog ==

= 2.1 (Released: May 21, 2020) =
* Updated: readme.txt and style.css as per the new requirements

= 2.0.1 (Released: April 12, 2019) =
* Bug Fixed: Animation issue in media player

= 2.0 (Released: January 02, 2019) =
* Added: Theme support for block styles
* Added: Theme support for full and wide align images
* Added: Theme support for responsive embeds
* Added: Theme support for custom editor font sizes
* Added: Theme support for custom color scheme
* Bug Fixed: Footer border issue
* Updated: Colors
* Updated: Rtl Styles
* Updated: Metabox UI
* Updated: Readme file as per new requirement

= 1.0  (Released: August 17, 2018) =
* Initial release

== Copyright ==

Pop Rock WordPress Theme, Copyright 2018 Catchthemes.com
Pop Rock is distributed under the terms of the GNU General Public License v3
