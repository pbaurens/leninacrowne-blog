<?php
/**
 * Components functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Pop Rock
 *
 *
 * Pop Rock Pro only works with parent theme My Music Band  1.2.1 or later.
 */

$my_theme = wp_get_theme('my-music-band');
if ( version_compare( $my_theme->get( 'Version' ), '1.2.2', '<' ) ) {
	require trailingslashit( get_stylesheet_directory() ) . '/inc/back-compat.php';
	return;
}

/**
 * Loads the child theme textdomain.
 */
function pop_rock_setup() {
    load_child_theme_textdomain( 'pop-rock', get_stylesheet_directory() . '/languages' );

    add_editor_style( array(
		'assets/css/editor-style.css',
		my_music_band_fonts_url(),
		trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'assets/css/font-awesome/css/font-awesome.css' )
	);
}
add_action( 'after_setup_theme', 'pop_rock_setup' );


/**
 * Enqueue scripts and styles.
 */
function pop_rock_scripts() {
	/* If using a child theme, auto-load the parent theme style. */
	if ( is_child_theme() ) {
		wp_enqueue_style( 'pop-rock-style', trailingslashit( esc_url( get_template_directory_uri() ) ) . 'style.css' );
	}

	wp_dequeue_script( 'my-music-band-block-style' ); 
	wp_deregister_script( 'my-music-band-block-style' );

	/* Always load active theme's style.css. */
	wp_enqueue_style( 'pop-rock-style', get_stylesheet_uri() );

	// Theme block stylesheet.
	wp_enqueue_style( 'pop-rock-block-style', get_theme_file_uri( '/assets/css/blocks.css' ), array( 'pop-rock-style' ), '1.0' );
}
add_action( 'wp_enqueue_scripts', 'pop_rock_scripts', 100 );

/**
 * Load Metabox
 */

require trailingslashit( get_stylesheet_directory() ) . '/inc/metabox/metabox.php';

/**
 * Load Customizer Options
 */
require trailingslashit( get_stylesheet_directory() ) . 'inc/customizer.php';

/**
 * Load Upgrade to pro button
 */
require trailingslashit( get_stylesheet_directory() ) . 'class-customize.php';

/**
 * Parent theme override functions
 */
require trailingslashit( get_stylesheet_directory() ) . 'inc/override-parent.php';
