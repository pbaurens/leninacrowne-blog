<?php
/**
 * Pop Rock Theme Customizer
 *
 * @package Pop Rock
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function pop_rock_customize_register( $wp_customize ) {
	$wp_customize->remove_section( 'upgrade_button' );

	// Important Links.
	class Pop_Rock_Important_links extends WP_Customize_Control {
		public $type = 'important-links';

		public function render_content() {
			// Add Theme instruction, Support Forum, Changelog, Donate link, Review, Facebook, Twitter, Google+, Pinterest links.
			$important_links = array(
				'theme_instructions' => array(
					'link'  => esc_url( 'https://catchthemes.com/theme-instructions/pop-rock/' ),
					'text'  => esc_html__( 'Theme Instructions', 'pop-rock' ),
					),
				'support' => array(
					'link'  => esc_url( 'https://catchthemes.com/support/' ),
					'text'  => esc_html__( 'Support', 'pop-rock' ),
					),
				'changelog' => array(
					'link'  => esc_url( 'https://catchthemes.com/changelogs/pop-rock-theme/' ),
					'text'  => esc_html__( 'Changelog', 'pop-rock' ),
					),
				'facebook' => array(
					'link'  => esc_url( 'https://www.facebook.com/catchthemes/' ),
					'text'  => esc_html__( 'Facebook', 'pop-rock' ),
					),
				'twitter' => array(
					'link'  => esc_url( 'https://twitter.com/catchthemes/' ),
					'text'  => esc_html__( 'Twitter', 'pop-rock' ),
					),
				'gplus' => array(
					'link'  => esc_url( 'https://plus.google.com/+Catchthemes/' ),
					'text'  => esc_html__( 'Google+', 'pop-rock' ),
					),
				'pinterest' => array(
					'link'  => esc_url( 'http://www.pinterest.com/catchthemes/' ),
					'text'  => esc_html__( 'Pinterest', 'pop-rock' ),
					),
			);

			foreach ( $important_links as $important_link ) {
				echo '<p><a target="_blank" href="' . $important_link['link'] . '" >' . $important_link['text'] . ' </a></p>'; // WPCS: XSS OK.
			}
		}
	}

	// Important Links.
	$wp_customize->add_section( 'my_music_band_important_links', array(
		'priority'      => 999,
		'title'         => esc_html__( 'Important Links', 'pop-rock' ),
	) );

	// Has dummy Sanitizaition function as it contains no value to be sanitized.
	my_music_band_register_option( $wp_customize, array(
			'name'              => 'my_music_band_important_links',
			'sanitize_callback' => 'sanitize_text_field',
			'custom_control'    => 'Pop_Rock_Important_links',
			'label'             => esc_html__( 'Important Links', 'pop-rock' ),
			'section'           => 'my_music_band_important_links',
			'type'              => 'my_music_band_important_links',
		)
	);
	// Important Links End.
}
add_action( 'customize_register', 'pop_rock_customize_register', 20 );
