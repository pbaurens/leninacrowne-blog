<?php
/**
 * Pop Rock back compat functionality
 *
 * Prevents Pop Rock from running if the parent theme version is
 * prior to 1.2.2
 * since this theme is not meant to be backward compatible beyond that and
 * relies on functions and markup changes introduced in 1.2.2
 *
 * @since Pop Rock 1.0
 */

/**
 * Prevent switching to Pop Rock on old versions of Parent theme.
 *
 * Switches to the default theme.
 *
 * @since Pop Rock 1.0
 */
function pop_rock_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'pop_rock_upgrade_notice' );
}
add_action( 'after_switch_theme', 'pop_rock_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * @since Pop Rock 1.0
 */
function pop_rock_upgrade_notice() {
	$my_theme = wp_get_theme('my-music-band');
	$message = sprintf( __( 'Pop Rock requires at least My Music Band version 1.2.2 You are running version %s. Please upgrade and try again.', 'pop-rock' ), $my_theme->get( 'Version' ) );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on My Music Band versions prior to 1.2.2
 *
 * @since Pop Rock 1.0
 */
function pop_rock_customize() {
	$my_theme = wp_get_theme('my-music-band');
	wp_die(
		sprintf( __( 'Pop Rock requires at least My Music Band version 1.2.2 You are running version %s. Please upgrade and try again.', 'pop-rock' ), $my_theme->get( 'Version' ) ), '', array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'pop_rock_customize' );

/**
 * Prevents the Theme Preview from being loaded on My Music Band versions prior to * 1.2.2
 *
 * @since Pop Rock 1.0
 *
 * @global string $wp_version WordPress version.
 */
function pop_rock_preview() {
	$my_theme = wp_get_theme('my-music-band');
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Pop Rock requires at least My Music Band version 1.2.2 You are running version %s. Please upgrade and try again.', 'pop-rock' ), $my_theme->get( 'Version' ) ) );
	}
}
add_action( 'template_redirect', 'pop_rock_preview' );
